package kayttaytymiset;
import lejos.robotics.subsumption.Behavior;
import logistiikkarobotti.Liikkuminen;

public class Lastaus implements Behavior {
	private Liikkuminen liikkuminen;

	//Täällä suoritetaan metodien avulla haettavan objektin lähestyminen, kiinniotto ja tunnistaminen
	public Lastaus(Liikkuminen liikkuminen) {
		this.liikkuminen = liikkuminen;
	}
	@Override
	public boolean takeControl() {
		return false;
	}

	@Override
	public void action() {
		//Robotin saapuessa määrättyyn pisteeseen siirtyy ohjaus tänne, jolloin robotti hitaasti etenee kohti objektia
		//kunnes tunnistaa sen IR-anturin avulla, sulkee kouran ja tarkistaa kiinnioton onnistumisen
		//Kun objekti on saatu onnistuneesti napattua kouraan, siirrytään ajamaan jättöpisteelle (Waypoint2)

	}

	@Override
	public void suppress() {
	}

}