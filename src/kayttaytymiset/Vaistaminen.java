package kayttaytymiset;

import lejos.robotics.subsumption.Behavior;
import logistiikkarobotti.IRanturi;
import logistiikkarobotti.Liikkuminen;

public class Vaistaminen implements Behavior {
	private IRanturi anturi;
	private Liikkuminen liikkuminen;
	private volatile boolean suppressed = false;

	// Täällä suoritetaan metodien avulla väistäminen mikäli robotti havaitsee
	// toisella IR-anturillaan jonkin esteen
	public Vaistaminen(IRanturi anturi, Liikkuminen liikkuminen) {
		this.anturi = anturi;
		this.liikkuminen = liikkuminen;
	}

	@Override
	public boolean takeControl() {
		if (anturi.tarkistaEste()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void action() {
		suppressed = false;		
		while (liikkuminen.liikkeessa() && !suppressed)
			Thread.yield();
		if (suppressed) {
			liikkuminen.vaista();
		}
	}

	@Override
	public void suppress() {
		suppressed = true;
	}
}