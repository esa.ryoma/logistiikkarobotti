package kayttaytymiset;
import lejos.robotics.subsumption.Behavior;
import logistiikkarobotti.Liikkuminen;

public class Navigointi implements Behavior {
	private volatile boolean suppressed = false;
	private Liikkuminen liikkuminen;
	//Täällä suoritetaan metodien avulla navigointi eli eteenpäin ja taaksepäin ajaminen sekä kääntyminen
	public Navigointi(Liikkuminen liikkuminen) {
		this.liikkuminen = liikkuminen;
	}
	
	@Override
	public boolean takeControl() {
		return true;
	}

	@Override
	public void action() {
		//Main-luokassa luodut navi-, pilot- ja chassis-oliot haetaan tänne ja suoritetaan kartan alueen 
		//sisäpuolella tapahtuva ajo
		//Mikäli robotti havaitsee edessään objektin tai se saapuu sille annettuun noutopisteeseen (Waypoint1), 
		//siirtää se kontrollin toiselle käyttäytymiselle
		suppressed = false;
		
	}

	@Override
	public void suppress() {
		suppressed = true;
	}

}
