package logistiikkarobotti;

import lejos.robotics.RegulatedMotor;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.geometry.Line;
import lejos.robotics.geometry.Rectangle;
import lejos.robotics.localization.BeaconPoseProvider; // voi olla turha, kunhan ollaan todennettu beaconeiden toiminta
import lejos.robotics.mapping.LineMap;
import lejos.robotics.navigation.MovePilot;
import lejos.robotics.navigation.Navigator;
import lejos.robotics.navigation.Pose;
import lejos.robotics.navigation.Waypoint;
import lejos.robotics.pathfinding.Path;
import lejos.robotics.pathfinding.PathFinder;
import lejos.robotics.pathfinding.ShortestPathFinder;
import lejos.utility.Delay;

import java.util.ArrayList;

import lejos.hardware.Sound;

public class Liikkuminen {
	private final double WHEEL_DIAMETER = 3.25f; // renkaan halkaisija
	private final double WHEEL_OFFSET = 7.64f; // renkaan keskipisteen etäisyys robotin keskipisteestä

	private Wheel wheel1;
	private Wheel wheel2;

	private Chassis chassis;
	private MovePilot pilot;

	private Rectangle suorakulmio;
	
	
	/**
	 * A Line array which has all the walls and obstacles in the world. Notice: Lines can be used to make "invisible" obstacles which don't exist in the real world.
	 */
	private Line[] janat;
	
	/**
	 * This LineMap contains the walls & obstacles (as Lines) and the area in EV3's inner world (as a rectangle).
	 */
	private LineMap kartta;
	private Navigator navi;
	
	private Koura koura;
	
	private PathFinder polunEtsijä;
	
	private Pose peruutusPose;
	private IRanturi iranturi; // voi olla turha, kunhan todennetaan beaconeiden toiminta
	//private Compass compass;
	private MajakkaPaikannin mp; 
	// Sisältää metodin locate()
	
	/**
	 * A list to save the angles of beacons
	 * in relation to EV3 to use in triangulation
	 */
	private ArrayList<Double> anglesToBeacons = new ArrayList<>();
	
	Pose tempPose;
	
	private boolean liikkeessa = false;

	
	public Liikkuminen(RegulatedMotor oikeaMoottori, RegulatedMotor vasenMoottori, Koura koura) {
		this.wheel1 = WheeledChassis.modelWheel(oikeaMoottori, WHEEL_DIAMETER).offset(WHEEL_OFFSET);
		this.wheel2 = WheeledChassis.modelWheel(vasenMoottori, WHEEL_DIAMETER).offset(-WHEEL_OFFSET);
		this.chassis = new WheeledChassis(new Wheel[] { wheel1, wheel2 }, WheeledChassis.TYPE_DIFFERENTIAL);
		this.pilot = new MovePilot(chassis);
		this.navi = new Navigator(pilot, chassis.getPoseProvider());
		//Koura
		this.koura = koura;
		//compass = new Compass(pilot);
		this.mp = new MajakkaPaikannin(iranturi);
	}

	/**
	 * Constructs the map for EV3's ShortestPathFinder, and finally uses lengthenLines to increase every wall on the map. <br>
	 * EV3's inner world is limited by a rectangle. The lower left-hand corner is at 0,0 and x,y is it's top right-hand corner. 
	 * @param seinat A Line array which has all the walls and obstacles in the world. Notice: Lines can be used to make "invisible" obstacles which don't exist in the real world.
	 * @param x The X-coordinate of the top right-hand corner of the EV3's inner world rectangle. 
	 * @param y The Y-coordinate of the top right-hand corner of the EV3's inner world rectangle.
	 */
	public void teeKartta(Line[] seinat, int x, int y) {
		this.suorakulmio = new Rectangle(0, 0, x, y);
		this.janat = seinat;
		// Kartan luonti annetuilla janoilla ja rajaavalla suorakulmiolla
		kartta = new LineMap(janat, suorakulmio);
		// Pathfinder
		// Polunetsijän luonti (parametrinä luomamme LineMap-kartta)
		this.polunEtsijä = new ShortestPathFinder(kartta);
		((ShortestPathFinder) polunEtsijä).lengthenLines(10);
	}

	/**
	 * This method uses a navigator to guide EV3 through several Waypoints.<br>
	 * The method uses ShortestPathFinder to find the shortest route between the EV3's current position to the next Waypoint.
	 * Current position is received through the navigator's PoseProvider
	 * @param kohteet An array of Waypoint objects. 
	 */
	public void aja (Waypoint[] kohteet) {
		Path polku = null;
		for (int i = 0; i < kohteet.length; i++) {
			try {
				polku = polunEtsijä.findRoute(navi.getPoseProvider().getPose(), kohteet[i]); // varaudu poikkeukseen!
				printPose();
			} catch (Exception e) {
				e.printStackTrace();
			}
			navi.setPath(polku);
			navi.followPath();
			if (pilot.isMoving()) {
				liikkeessa = true;
			}
			navi.waitForStop();
			if (i == 0) {
				koura.kouraSulje();
				//peruuta();
			} else if (i == 1) {
				koura.kouraAvaa();
				//peruuta();
			}
			liikkeessa = false;
		}
		try {
			printPose();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void peruuta() {
		pilot.travel(-20);
	}
	
	public void vaista() {
		pilot.rotate(90);
		pilot.travel(10);
	}
	
	/* 
	 * Kompassin käytössä käytettyjä metodeja
	 * public void kalibroiKompassi() {
		compass.aloitaKalibrointi();
		pyori720();
		compass.lopetaKalibrointi();
	}
	
	
	public void pyori720() {
		chassis.setAngularSpeed(36);
		pilot.rotate(720);
	}
	*/
	
	public void pyori360() {
		pilot.rotate(360);
	}
	
	public void eteen100() {
		pilot.travel(100);
	}
	

	
	public MovePilot getPilot() {
		return pilot;
	}
	public boolean liikkeessa() {
		return this.liikkeessa;
	}
	
	public void setIRanturi(IRanturi iranturi) {
		this.iranturi = iranturi;
	}
	
	/**
	 * Prints the pose of EV3. For testing purposes.<br>
	 * First line: Pose calculated by the navigator's inner odometry<br>
	 * Second line: Pose calculated by pose-correcting measures.
	 */
	public void printPose(){
		System.out.println("Robotin pose: " + navi.getPoseProvider().getPose()); 
		try {
			System.out.println("Mitattu pose: " + getPose());
		} catch (NullPointerException e) {
			//e.printStackTrace();
			Sound.buzz();
			System.out.print("Can't find 3 beacons");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.print("Something wrong when scanning for beacons");
		}
		
	}
	
	public Pose getPose() throws Exception {
		double a1 = 0, a2 = 0, a3 = 0; // for saving angles
		anglesToBeacons = mp.locate();
		 
		/* a1 = anglesToBeacons.get(0);
		a2 = anglesToBeacons.get(1);
		a3 = anglesToBeacons.get(2);
		*/
		tempPose = iranturi.getBeaconTriangle().calcPose(a1, a2, a3);
		return tempPose;
	}

	public MajakkaPaikannin getMajakkaPaikannin() {
		return this.mp;
	}
}
