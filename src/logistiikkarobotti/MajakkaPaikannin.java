// EI KÄYTÖSSÄ LOPULLISESSA PROJEKTISSA
// Yritys käyttää kolmea ir-kaukosäädintä majakoina, joita robotti käyttäisi lokaationsa triangulointiin. Kaatui siihen, ettei kaukosäätimiltä saatu signaalia.
package logistiikkarobotti;

import java.util.ArrayList;

import lejos.robotics.localization.BeaconLocator;
import lejos.utility.Delay;

public class MajakkaPaikannin implements BeaconLocator {
	private IRanturi beaconIR;
	private float sampleIR[];
	
	public MajakkaPaikannin(IRanturi beaconIR) {
		this.beaconIR = beaconIR;
	}
	
	
	/**
	 * This method takes a sample of the IR beacons surrounding it and returns their angles relative to the EV3. The direction the EV3 is facing is 0, and the angle grows counter clock-wise: directly to the left is 90, behind is 180, right is 270 degrees.
	 * @return Returns an ArrayList of Doubles. <br>
	 * List[0] - Angle to beacon on channel 0<br>
	 * List[1] - Angle to beacon on channel 1<br>
	 * List[2] - Angle to beacon on channel 2<br>
	 * Returns null if locates less than 3 beacons. 
	 */
	public ArrayList<Double> locate() {
		System.out.print("Taking sample...");
		beaconIR.getSampleProvider().fetchSample(sampleIR, 0);
		/* Sample contains up to four pairs of elements
		* Each pair gives bearing of and distance to the beacn
		* First pair - Channel 1, Second pair - Channel 2 etc.
		* If no beacon is deteced both bearing is set to zero, and distane to positive infinity
		*/
		System.out.print("Trying to print sample...");
		for (float sample : sampleIR) {
			System.out.print(sample);
		}
		Delay.msDelay(5000);
		
		// Let's check the number of samples not having POSITIVE_INFINITY
		double tempDouble = 0;
		ArrayList<Double> angles = new ArrayList<>();
		
		for (int i = 1; i <= sampleIR.length + 1; i += 2) {
			if (sampleIR[i] < Float.POSITIVE_INFINITY) {
				// If Distance is not POSITIVE_INFINITY
				// let's add the angle of that beacon to list
				tempDouble = sampleIR[i-1];
				angles.add(tempDouble);
			}
		}
		
		// Triangulation impossible if less than 3 good samples 
		if (angles.size() < 3) {
			return null;
		}
		
		// Triangulation possible, enough samples
		// Slot 0: Beacon Channel 0
		// Slot 1: Beacon Channel 1
		// Slot 2: Beacon Channel 2
		return angles;
		
	}

	
	
	
	
	

}
