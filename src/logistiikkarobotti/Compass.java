// EI KÄYTÖSSÄ LOPULLISESSA PROJEKTISSA
// Yritys käyttää compassi-moduulia robotin paikantamiseksi koordinaatistossa
// Kaatui siihen, että signaalia ei saatu kompassimoduulilta lainkaan
package logistiikkarobotti;

import lejos.robotics.DirectionFinderAdapter;
import lejos.robotics.localization.CompassPoseProvider;
import lejos.robotics.navigation.MovePilot;
import lejos.robotics.navigation.Pose;
import lejos.hardware.sensor.HiTechnicCompass;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;

public class Compass {
	private Port compassPort = LocalEV3.get().getPort("S4");
	private CompassPoseProvider cpp; 
	private DirectionFinderAdapter dfa;
	private HiTechnicCompass htc = new HiTechnicCompass(compassPort); // htc implementoi SampleProvider IF:ää
	private Pose startPose = new Pose(0, 0, 0);
	
	public Compass(MovePilot pilot) {
		dfa = new DirectionFinderAdapter(htc); // (SampleProvider sp) HiTechnicCompass implementoi
		cpp = new CompassPoseProvider(pilot, dfa); // (MoveProvider mp MovePilot implementoi. DirectionFinder df DirectionFinderAdapter implementoi)
		dfa.resetCartesianZero();
	}

	public Pose getPose() {
		return cpp.getPose();
	}
	
	public void aloitaKalibrointi() {
		dfa.startCalibration();
	}
	
	public void lopetaKalibrointi() {
		dfa.stopCalibration();
	}
	
}
