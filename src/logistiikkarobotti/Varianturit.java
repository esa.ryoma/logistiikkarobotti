package logistiikkarobotti;

import lejos.hardware.Button;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.SensorModes;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class Varianturit {

	private SensorModes sensor;
	private SensorModes sensor2;
	private SampleProvider colorProvider;
	private SampleProvider colorProvider2;
	private float[] sample;
	private float[] sample2;

	private int calibrated_color[] = new int[3];
	private int calibrated_color2[] = new int[3];

	public Varianturit(Port port, Port port2) {
		this.sensor = new EV3ColorSensor(port);
		this.sensor2 = new EV3ColorSensor(port2);
		this.colorProvider = ((EV3ColorSensor) sensor).getRGBMode();
		this.colorProvider2 = ((EV3ColorSensor) sensor2).getRGBMode();
		this.sample = new float[colorProvider.sampleSize()];
		this.sample2 = new float[colorProvider2.sampleSize()];
	}

	// Ajatus on, että kalibrointi ajetaan aina robotin käynnistyessä mainissa
	// Värianturi asetetaan halutulle värille, painetaan enter, ja robotti kysyy
	// varmennuksen RGB:stä
	public void kalibroi() {
		do {
			System.out.println("Aseta kamera värinauhalle ja paina Enter");
			Delay.msDelay(1000);
			Button.ENTER.waitForPressAndRelease();
			calibrated_color = fetchRGB(this.colorProvider, this.sample);
			calibrated_color2 = fetchRGB(this.colorProvider2, this.sample2);
			printtaaVari();
			System.out.println("Enter: OK! Cancel: Takaisin");
			Button.waitForAnyEvent();
		} while (Button.ESCAPE.isDown());
		printtaaVari();
		Delay.msDelay(3000);
	}

	// Palauttaa arvon false, jos väriä ei havaita
	// True, jos väri havaitaan

	public boolean test() {
		// Haetaan sensorilta RGB-arvot
		int rgb[] = new int[3];
		
		rgb = fetchRGB(this.colorProvider, this.sample);
		int sensorR = rgb[0];
		int sensorG = rgb[1];
		int sensorB = rgb[2];
		
		// Lasketaan etäisyys kalibroidusta väristä.
		double distance_from_color = Math.sqrt(Math.pow(sensorR - calibrated_color[0], 2)
				+ Math.pow(sensorG - calibrated_color[1], 2) + Math.pow(sensorB - calibrated_color[2], 2));
		
		// System.out.println("Distance from color: " + (int)distance_from_color);

		// Jos saatu värietäisyys on isompi kuin 20, palautetaan VARITON joka on 0;
		if (distance_from_color <= 20) {
			// System.out.print("Rajaviiva!");
			return true;
		} else {
			// System.out.println("Ei mitään");
			return false;
		}
	}
	
	public boolean test2() {
		int rgb2[] = new int[3];
		rgb2 = fetchRGB(this.colorProvider2, this.sample2);
		int sensorR2 = rgb2[0];
		int sensorG2 = rgb2[1];
		int sensorB2 = rgb2[2];
		double distance_from_color2 = Math.sqrt(Math.pow(sensorR2 - calibrated_color2[0], 2)
				+ Math.pow(sensorG2 - calibrated_color2[1], 2) + Math.pow(sensorB2 - calibrated_color2[2], 2));
		if (distance_from_color2 <= 20) {
			// System.out.print("Rajaviiva!");
			return true;
		} else {
			// System.out.println("Ei mitään");
			return false;
		}
	}
	

	// getCalibratedColor() palauttaa kolmialkioisen INT[]-taulukon {R,G,B}
	public int[] getCalibratedColor(int i) {
		if (i == 1) {
			return calibrated_color;
		} else if (i == 2) {
			return calibrated_color2;
		}
		return null;
	}

	// printtaaVari() tulostaa juuri nyt näkemänsä värin
	public void printtaaVari() {
		int rgb[] = new int[3];
		rgb = this.calibrated_color;
		System.out.println("R: " + rgb[0] + " G: " + rgb[1] + " B: " + rgb[2]);
		rgb = this.calibrated_color2;
		System.out.println("R: " + rgb[0] + " G: " + rgb[1] + " B: " + rgb[2]);
	}

	// fetchRGB hakee RGB-float-arvot ja laskee ne kokonaisluvuiksi
	public int[] fetchRGB(SampleProvider provider, float[] sample) {
		int rgb[] = new int[3];
		provider.fetchSample(sample, 0);
		// Skaalaa arvot 0...1 kokonaisluvuiksi välille 0...255
		int r = (int) Math.round(sample[0] * 765);
		int g = (int) Math.round(sample[1] * 765);
		int b = (int) Math.round(sample[2] * 765);
		rgb[0] = r;
		rgb[1] = g;
		rgb[2] = b;
		return rgb;
	}
}
