package logistiikkarobotti;

import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.Port;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.geometry.Line;
import lejos.robotics.navigation.Waypoint;
import lejos.utility.Delay;

public class Main {

	public static void main(String[] args) {
		// KOURAN ON OLTAVA LÄHTÖTILANTEESSA AUKI!!!
		RegulatedMotor kouraMoottori = new EV3MediumRegulatedMotor(MotorPort.A);
		RegulatedMotor oikeaMoottori = new EV3LargeRegulatedMotor(MotorPort.B);
		RegulatedMotor vasenMoottori = new EV3LargeRegulatedMotor(MotorPort.C);
		
		//Portit
		Port vaistoIRPort = LocalEV3.get().getPort("S1");
		Port kouraIRPort = LocalEV3.get().getPort("S4");
		
		// Muuttujat
		final int IRSENSORMODE_DISTANCE = 0;
		final int IRSENSORMODE_SEEK = 1;
		
		//Classit
		Koura koura = new Koura(kouraMoottori);
		Liikkuminen liikkuminen = new Liikkuminen(vasenMoottori, oikeaMoottori, koura);
		
		IRanturi iranturi = new IRanturi(vaistoIRPort, liikkuminen.getPilot(), IRSENSORMODE_DISTANCE);
		//IRanturi kouraIR = new IRanturi(kouraIRPort, liikkuminen.getPilot(), IRSENSORMODE_DISTANCE);
		IRanturi beaconIR = new IRanturi(kouraIRPort, liikkuminen.getPilot(), IRSENSORMODE_SEEK);
		
		
		
		
		
		Line[] janat = new Line[4];
		janat[0] = new Line(-10, -10, 260, -10); // Alaviiva
		janat[1] = new Line(260, -10, 260, 210); // Oikea viiva
		janat[2] = new Line(-10, 210, 260, 210); // Yläviiva
		janat[3] = new Line(-10, -10, -10, 210); // Vasen viiva
		
		//Noutokolo
		/* janat[4] = new Line(40, 0, 40, 50);
		janat[5] = new Line(50, 0, 50, 50);
		janat[6] = new Line(40, 50, 50, 50);
		janat[7] = new Line(70, 0, 70, 50);
		janat[8] = new Line(80, 0, 80, 50);
		janat[9] = new Line(70, 50, 80, 50);
		*/
		
		/* Kartan mukainen noutokolo
		janat[4] = new Line(120, 0, 120, 50); 
		janat[5] = new Line(130, 0, 130, 50);
		janat[6] = new Line(120, 50, 130, 50);
		janat[7] = new Line(150, 0, 150, 50);
		janat[8] = new Line(160, 0, 160, 50);
		janat[9] = new Line(150, 50, 160, 50);
		*/
		
		// Navi-liikkuminen
		
		liikkuminen.teeKartta(janat, 250, 200);
		liikkuminen.setIRanturi(beaconIR);
		
		Waypoint[] kohteet = new Waypoint[2];
		/*
		// Noutopiste
		kohteet[0] = new Waypoint(100, 0);
		// Jättöpiste
		//kohteet[1] = new Waypoint(100, 0);
		liikkuminen.aja(kohteet);
		Delay.msDelay(4000);
		*/
		
		// kohteet[0] = new Waypoint(60, 10);
		//kohteet[1] = new Waypoint(20, 60);
		
		// Testipyörähdys
		//liikkuminen.pyori360();
		
		// Testi eteenpäin 100 cm
		//liikkuminen.eteen100();
		
		// Kompassin kalibrointi, tarvitsee ajaa vain kerran
		//liikkuminen.kalibroiKompassi();
		
		//kohteet[0] = new Waypoint(50, 15);
		//kohteet[1] = new Waypoint(0, 0);
		//liikkuminen.aja(kohteet);
	
		kohteet[0] = new Waypoint(50, 0);
		kohteet[1] = new Waypoint(100, 0);
		liikkuminen.aja(kohteet);
		Delay.msDelay(4000);
	}
}
