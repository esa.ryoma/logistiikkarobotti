package logistiikkarobotti;

import java.util.ArrayList;

import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.robotics.SampleProvider;
import lejos.robotics.geometry.Point;
import lejos.robotics.localization.*;
import lejos.hardware.sensor.SensorModes;
import lejos.robotics.navigation.MovePilot;
import lejos.robotics.navigation.MoveProvider;
import lejos.robotics.navigation.Pose;
import lejos.utility.Delay;
import java.util.Arrays;


/**
 * The IRanturi class handles taking samples with the IR sensors.
 * 
 * @author Team 10
 * @version 0.0000paskanakki
 *
 */
public class IRanturi {
	
	private SensorModes sensor;
	private SampleProvider sp;
	private float[] sampleIR; 
	/**
	 * Starting Point(x,y) for Beacon 1 on channel 0.
	 */
	private Point beacon1 = new Point(0, -30);
	
	/**
	 * Starting Point(x,y) for Beacon 2 on channel 1.
	 */
	private Point beacon2 = new Point(0, 90);
	
	/**
	 * Starting Point(x,y) for Beacon 3 on channel 2.
	 */
	private Point beacon3 = new Point(90, 0);
	
	public static boolean este = false;
	public boolean koura = false;
	
		
	private BeaconTriangle bt = new BeaconTriangle(beacon1, beacon2, beacon3);
	// BeaconTriangle(Point beacon1, Point beacon2, Point beacon3) 
	// calcPose(double a1, double a2, double a3)
	// Triangulates the pose of a robot given three angles to the three beacons.
	// Returns: Pose

	/**
	 * Scans a room for beacons and identifies the angles to the beacons
	 */
	
	
	//private BeaconPoseProvider bpp; 
	// BeaconPoseProvider(BeaconLocator bl, BeaconTriangle bt, MoveProvider mp)
	
	
	/*
	 * VANHA KONSTRUKTORI ILMAN BEACONEITA
	public IRanturi(Port port, MovePilot pilot) {
		try{
			this.sensor = new EV3IRSensor(port);
			this.sp = ((EV3IRSensor) sensor).getDistanceMode();
			this.sampleIR = new float[sp.sampleSize()];
		} catch(Exception e) {
			e.printStackTrace();
			Delay.msDelay(5000);
			System.exit(0);
		}
		//this.distance = ((EV3IRSensor) sensor).getDistanceMode();
		//this.sampleIR = new float[distance.sampleSize()];
	}
	*/
	
	/**
	 * @param port The port the ir sensor is attached to, as a Port object
	 * @param pilot The MovePilot object used to move EV3
	 * @param mode 0: This IRanturi object uses DistanceMode. <br>1: This IRanturi object uses SeekMode. Also, the BeaconPoseProvider bpp is generated.
	 * 
	 */
	public IRanturi(Port port, MovePilot pilot, int mode) {
		// SensorMode == Distance
		if (mode == 0) {
			try{
				this.sensor = new EV3IRSensor(port);
				this.sp = ((EV3IRSensor) sensor).getDistanceMode();
				this.sampleIR = new float[sp.sampleSize()];
				//this.bpp = null;
			} catch(Exception e) {
				e.printStackTrace();
				Delay.msDelay(5000);
				System.exit(0);
			}
		}
		// SensorMode == Seek
		else if (mode == 1) {
			try{
				this.sensor = new EV3IRSensor(port);
				this.sp = ((EV3IRSensor) sensor).getSeekMode();
				this.sampleIR = new float[sp.sampleSize()];
				//bpp = new BeaconPoseProvider(mp, bt, pilot, 2); // Viimeinen int - kuinka monen liikkeen jälkeen skannataan
			} catch(Exception e) {
				e.printStackTrace();
				Delay.msDelay(5000);
				System.exit(0);
			}
		}
	}
	
	/**
	 * Checks if there is something at below 15 cm distance from the IR sensor.
	 * @return True, if there is something closer than 15 cm.<br>
	 * False, if there is nothing.
	 */
	public boolean tarkistaEste() {
		sp.fetchSample(sampleIR, 0);
		if (sampleIR[0] < 15) {
				este = true;
		} else {
			este = false;
		}
		return este;
	}
	/**
	 * Checks if there is an item at below 5 cm distance from the IR sensor.
	 * @return True, if there is something closer than 5 cm.<br>
	 * False, if there is nothing.
 	 */
	public boolean tarkistaKoura() {
		sp.fetchSample(sampleIR, 0);
		if (sampleIR[0] < 5) {
			koura = true;
		} else {
			koura = false;
		}
		return koura;
	}

	/**
	 * @return This IRanturi's SampleProvider
	 */
	public SampleProvider getSampleProvider() {
		return sp;
	}
	
	/**
	 * Calculates EV3's pose using a BeaconLocator for angles to 3 beacons and BeaconTriangle's calcPose() method.
	 * @return The calculated Pose.
	 * @throws Exception BeaconLocator.locate() returns null if it cannot locate all 3 beacons. This will ruin the rest of the method, throwing an Exception.  
	 */
	
	
	public BeaconTriangle getBeaconTriangle() {
		return this.bt;
	}
	
	/* 
	 * public Pose getPose() {
	 * return bpp.getPose();
	 * }
	 */
	
	


}