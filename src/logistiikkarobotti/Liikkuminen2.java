package logistiikkarobotti;

import lejos.robotics.RegulatedMotor;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.navigation.MovePilot;

public class Liikkuminen2 {
	private final double WHEEL_DIAMETER = 3.25f; // renkaan halkaisija
	private final double WHEEL_OFFSET = 7.64f; // renkaan keskipisteen etäisyys robotin keskipisteestä

	private Wheel wheel1;
	private Wheel wheel2;

	private Chassis chassis;
	private MovePilot pilot;

	
	public Liikkuminen2(RegulatedMotor oikeaMoottori, RegulatedMotor vasenMoottori) {
		this.wheel1 = WheeledChassis.modelWheel(oikeaMoottori, WHEEL_DIAMETER).offset(WHEEL_OFFSET);
		this.wheel2 = WheeledChassis.modelWheel(vasenMoottori, WHEEL_DIAMETER).offset(-WHEEL_OFFSET);
		this.chassis = new WheeledChassis(new Wheel[] { wheel1, wheel2 }, WheeledChassis.TYPE_DIFFERENTIAL);
		this.pilot = new MovePilot(chassis);
	}
	
	public void peruuta() {
		pilot.backward();
	}
	
	public void eteen() {
		pilot.forward();
	}
	
	public void kaanny(int i) {
		pilot.rotate(i*30);
	}
	
	public void stop() {
		pilot.stop();
	}
}
