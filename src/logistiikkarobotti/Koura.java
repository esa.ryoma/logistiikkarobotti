package logistiikkarobotti;

import lejos.robotics.RegulatedMotor;

public class Koura {
	
	private RegulatedMotor koura;
	private boolean kouraOnAuki = true; //KOURA ON AUKI LÄHTÖTILANTEESSA

	public Koura(RegulatedMotor koura) {
		this.koura = koura;
	}

	public void kouraAvaa() {
		if (kouraOnAuki == false) {
			this.koura.rotate(-320);
			this.koura.waitComplete();
			kouraOnAuki = true;
		}
	}

	public void kouraSulje() {
		if (kouraOnAuki == true) {
			this.koura.rotate(320);
			this.koura.waitComplete();
			kouraOnAuki = false;
		}
	}
}
